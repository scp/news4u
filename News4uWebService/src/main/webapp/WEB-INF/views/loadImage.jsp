<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/static/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="/static/js/jquerypp.custom.js"></script>
<script type="text/javascript" src="/static/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/static/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/static/js/jquery.bookblock.js"></script>
<script type="text/javascript" src="/static/js/page.js"></script>
<script>
    $(function () {

        Page.init();

    });
</script>
<!-- include jQuery -->

<!-- Include the plug-in -->
<script src="/static/js/jquery.wookmark.js"></script>

<!-- Once the page is loaded, initalize the plug-in. -->
<script type="text/javascript">
    var handler = null;
    var page = 1;
    var isLoading = false;
    var apiURL = '/category/'

    // Prepare layout options.
    var options = {
        autoResize:true, // This will auto-update the layout when the browser window is resized.
        container:$('#tiles'), // Optional, used for some extra CSS styling
        offset:2, // Optional, the distance between grid items
        itemWidth:310 // Optional, the width of a grid item
    };

    /**
     * When scrolled all the way to the bottom, add more tiles.
     */
    function onScroll(event) {
        // Only check when we're not still waiting for data.
        if (!isLoading) {
            // Check if we're within 100 pixels of the bottom edge of the browser window.
            var closeToBottom = ($(window).scrollTop() + $(window).height() > $(document).height() - 100);
            if (closeToBottom) {
                loadData('Top Stories');
            }
        }
    }
    ;

    /**
     * Refreshes the layout.
     */
    function applyLayout() {
        // Clear our previous layout handler.
        if (handler) handler.wookmarkClear();

        // Create a new layout handler.
        handler = $('#tiles li');
        handler.wookmark(options);
    }
    ;

    /**
     * Loads data from the API.
     */
    function loadData(category) {
        isLoading = true;
        $('#loaderCircle').show();

        $.ajax({
            url:'http://localhost:8080/category/' + category,
            dataType:'json',
            data:{id:1, count:5, page:page}, // Page parameter to make sure we load new data
            success:onLoadData
        });
    }

    /**
     * Receives data from the API, creates HTML for images and updates the layout
     */
    function onLoadData(data) {
        isLoading = false;
        $('#loaderCircle').hide();
        page++;
        var html = '';
        var i = 0, length = data.length, image;
        for (; i < length; i++) {
            image = data[i].image;
            html += '<li>';
            html += '<a class="example2" href="' + image.imageSrc + '"><img  class="ref" src="' + image.imageSrc + '" width="300" height="' + Math.round(image.height / image.width * 300) + '">';
            html += '<div class="bubble"><p><center><font face="Helvetica" size="4px"></center>' + data[i].title + '</font></p></div></a>';
            html += '</li>';
        }

        // Add image HTML to the page.
        $('#tiles').append(html);

        // Apply layout.
        applyLayout();



    }
    ;

    $(document).ready(new function () {
        // Capture scroll event.
        //$(document).bind('scroll', onScroll);

        // Load first data from the API.


        loadData('Top Stories');


    });
</script>
