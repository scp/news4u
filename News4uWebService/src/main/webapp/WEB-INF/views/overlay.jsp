<%--
  Created by IntelliJ IDEA.
  User: prashanth.v
  Date: 3/3/13
  Time: 10:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        .details {
            margin: 15px 20px;
        }

         h4 {
            font: 300 16px 'Helvetica Neue', Helvetica, Arial, sans-serif;
            line-height: 140%;
            color: #fff;
            text-shadow: 1px 1px 0 rgb(0, 0, 0);
        }

          p {
            font: 300 12px 'Lucida Grande', Tahoma, Verdana, sans-serif;
            color: #aaa;
            text-shadow: 1px 1px 0 rgb(0, 0, 0);
        }

         a {
            text-decoration: none;
        }

            /*General Mosaic Styles*/
        .mosaic-block {
            float: left;
            position: relative;
            overflow: hidden;
            width: 400px;
            height: 250px;
            margin: 10px;
            border: 1px solid #fff;
            border: 1px solid #fff;
            -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.5);
        }

        .mosaic-backdrop {
            display: none;
            position: absolute;
            top: 0;
            height: 100%;
            width: 100%;
            background: #111;
        }

        .mosaic-overlay {
            display: none;
            z-index: 5;
            position: absolute;
            width: 100%;
            height: 100%;
            background: #111;
        }

            /*** Custom Animation Styles (You can remove/add any styles below) ***/

        .fade .mosaic-overlay {
            opacity: 0;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=00)";
            filter: alpha(opacity = 00);

        }

        .bar .mosaic-overlay {
            bottom: -100px;
            height: 100px;

        }

        .bar2 .mosaic-overlay {
            bottom: -50px;
            height: 100px;
            opacity: 0.8;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
            filter: alpha(opacity = 80);
        }

        .bar2 .mosaic-overlay:hover {
            opacity: 1;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
            filter: alpha(opacity = 100);
        }

            /*** End Animation Styles ***/
    </style>
</head>

<body>
<div class="mosaic-block bar2">
    <a href="http://www.nonsensesociety.com/2010/12/i-am-not-human-portraits/" target="_blank" class="mosaic-overlay">
        <div class="details">
            <h4>I Am Not Human - Unique Self Portraits</h4><br/>

            <p>via the Nonsense Society (photo credit: Dan Deroches)</p>
        </div>
    </a>

    <div class="mosaic-backdrop"><img src="http://buildinternet.s3.amazonaws.com/projects/mosaic/desroches.jpg"/></div>
</div>
<div class="mosaic-block bar2">
    <a href="http://www.nonsensesociety.com/2010/12/i-am-not-human-portraits/" target="_blank" class="mosaic-overlay">
        <div class="details">
            <h4>I Am Not Human - Unique Self Portraits via alkj khkj kjkjhkjbkj kugku</h4><br/>

            <p>via the Nonsense Society (photo credit: Dan Deroches)</p>
        </div>
    </a>

    <div class="mosaic-backdrop"><img src="http://buildinternet.s3.amazonaws.com/projects/mosaic/desroches.jpg"/></div>
</div>
</div>

<script type="text/javascript" src="/static/js/jquery-1.9.1.js"></script>

<script src="/static/js/mosaic.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
        $('.bar2').mosaic({
            animation:'slide'        //fade or slide
        });
    });

</script>

</body>
</html>