var expanded = null;
var expandTimeout = null;

function expand()
{
        expanded.show(200);
}

jQuery.fn.thumbImg = function(img)
{
        return this.each(
                function()
                {
                        jQuery(this).mouseover(function(){
                                        if(expanded !=  jQuery($(".thumb-strip", this)))
                                        {
                                                if(expanded)
                                                {
                                                        expanded.hide();
                                                }
                                                clearTimeout(expandTimeout);
                                                expanded = jQuery($(".thumb-strip", this));
                                                expandTimeout = setTimeout("expand();", 500);
                                                //$(".thumb-strip", this).show(50);
                                        }
                                }).mouseout(function(){
                                                expanded.hide();
                                                clearTimeout(expandTimeout);
                                }).append(
                                        $("<DIV />").hide().
                                                addClass("thumb-strip")
                                                .append(
                                                                $("<IMG />").attr("src",img).addClass("thumb-strip-img")
                                                )
                                )
                }
        )
}