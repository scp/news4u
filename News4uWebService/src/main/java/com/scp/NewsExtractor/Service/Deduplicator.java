package com.scp.NewsExtractor.Service;

import com.scp.NewsExtractor.Model.Feed;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * User: prashanth.v
 * Date: 2/17/13
 * Time: 11:35 PM
 */
public class Deduplicator {

    private static Logger log = Logger.getLogger(Deduplicator.class);

    public List<String> getSimilarQuotes(ArrayList<Feed> feeds, String quoteText) throws CorruptIndexException, IOException {

        log.info("creating RAMDirectory");
        RAMDirectory idx = new RAMDirectory();
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_41, new StandardAnalyzer(Version.LUCENE_41));
        IndexWriter writer = new IndexWriter(idx, indexWriterConfig);
        List<Feed> rssFeeds = feeds;


        for (Feed feed : rssFeeds) {
            Document doc = new Document();
            doc.add(new Field("contents", feed.getDescription(), Field.Store.YES, Field.Index.ANALYZED));
            doc.add(new Field("id", feed.getLink().hashCode() + "", Field.Store.YES, Field.Index.ANALYZED));
            writer.addDocument(doc);
        }

        writer.close();

        IndexReader ir = IndexReader.open(idx);
        log.info("ir has " + ir.numDocs() + " docs in it");
        IndexSearcher is = new IndexSearcher(ir);


        MoreLikeThis mlt = new MoreLikeThis(ir);

        mlt.setMinTermFreq(1);
        mlt.setAnalyzer(new StandardAnalyzer(Version.LUCENE_41));
        mlt.setMinDocFreq(1);

        Reader reader = new StringReader(quoteText);

        Query query = mlt.like(reader, quoteText);
        TopDocs topDocs = is.search(query, 5);
        log.info("found " + topDocs.totalHits + " topDocs");

        List<String> foundQuotes = new ArrayList<String>();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = is.doc(scoreDoc.doc);

            String text = doc.get("contents");
            System.out.println("hello: " + text);
            foundQuotes.add(text);
        }

        return foundQuotes;
    }
}