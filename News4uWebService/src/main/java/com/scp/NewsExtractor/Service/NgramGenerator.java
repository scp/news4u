package com.scp.NewsExtractor.Service;

import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: prashanth.v
 * Date: 2/16/13
 * Time: 11:35 PM
 */
public class NgramGenerator {

    public Map<String, Integer> generateNgrams(Reader reader, int minNgram, int maxNgram) throws Exception {

        StandardTokenizer tokenizer = new StandardTokenizer(Version.LUCENE_41, reader);
        tokenizer.reset();
        ShingleFilter filter = new ShingleFilter(tokenizer, 2, 3);
        filter.setOutputUnigrams(false);
        CharTermAttribute charTermAttribute = filter.getAttribute(CharTermAttribute.class);
        ArrayList<String> gram = new ArrayList<String>();
        Map<String, Integer> ngramCount = new HashMap<String, Integer>();
        while (filter.incrementToken()) {
            String ngramWord = charTermAttribute.toString().toLowerCase().replaceAll("[^a-zA-Z- ]", "");
            Integer count = null;
            if ((count = ngramCount.get(ngramWord)) != null) {
                ngramCount.put(ngramWord, count + 1);
            } else {
                ngramCount.put(ngramWord, 1);
            }
        }
        return ngramCount;
    }

    public static void main(String[] args) throws Exception {
        NgramGenerator ngramGenerator = new NgramGenerator();
        ngramGenerator.generateNgrams(new StringReader("This is a string. So whats up?? bey"), 2, 3);
    }

}





