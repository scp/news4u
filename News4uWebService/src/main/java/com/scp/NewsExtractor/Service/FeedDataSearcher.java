package com.scp.NewsExtractor.Service;

import com.scp.NewsExtractor.Bean.Constants;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.ArrayList;

/**
 * User: prashanth.v
 * Date: 2/19/13
 * Time: 7:23 PM
 */
public class FeedDataSearcher {

    private IndexSearcher searcher = null;
    private QueryParser parser = null;

    public FeedDataSearcher(Directory indexDirectory) throws IOException {
        searcher = new IndexSearcher(DirectoryReader.open(indexDirectory));
    }

    public ArrayList<String> searchIstockIndexedData(String queryKeyword) throws IOException, ParseException {
        int hitsPerPage = 10;
        EnglishAnalyzer analyzer = new EnglishAnalyzer(Version.LUCENE_41, new CharArraySet(Version.LUCENE_41, Constants.STOP_LIST.getStringList(), true));
        ArrayList<String> urls = new ArrayList<String>();
        Query q = new QueryParser(Version.LUCENE_41, "searchData", analyzer).parse(queryKeyword);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
        searcher.search(q, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        System.out.println("Found " + hits.length + " hits.");
        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            System.out.println((i + 1) + ". " + d.get("url") + "\t" + d.get("description") + "\t" + d.get("rating"));
            urls.add(d.get("url"));

        }
        return urls;
    }

    public static void main(String[] args) throws Exception {
       /* FeedDataIndexer indexer = new FeedDataIndexer();
        //Directory indexDirectory=indexer.indexFeedData(IstockDataReader.readIstockData("C:\\Users\\prashanth.v\\Documents\\istock.xls"));
        Directory indexDirectory = new SimpleFSDirectory(new File(""));
        FeedDataSearcher feedDataSearcher = new FeedDataSearcher(indexDirectory);
        feedDataSearcher.searchIstockIndexedData("Best ranked scooters");   */

    }
}
