package com.scp.NewsExtractor.Service;

/**
 * User: prashanth.v
 * Date: 2/27/13
 * Time: 1:37 PM
 */


import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Scheduler for handling jobs
 */
@Service
public class RssSchedulingService {

    protected static Logger logger = Logger.getLogger("service");



    @Scheduled(cron="0 0 1 ? * SUN")
    @Async
    public void deleteIndex(){
        System.out.println("DELETING");
    }

}
