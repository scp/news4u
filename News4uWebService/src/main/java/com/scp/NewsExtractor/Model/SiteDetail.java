package com.scp.NewsExtractor.Model;

import java.util.ArrayList;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:15 PM
 */
public class SiteDetail {
    String name;
    ArrayList<UrlCategory> urlCategories;
    String baseUrl;
    String imgAttr,val;
    int linkAvailability;

    public SiteDetail(String name, ArrayList<UrlCategory> urlCategories, String baseUrl,String imgAttr,String val,int linkAvailability) {
        this.name = name;
        this.urlCategories = urlCategories;
        this.baseUrl = baseUrl;
        this.imgAttr=imgAttr;
        this.val=val;
        this.linkAvailability=linkAvailability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgAttr() {
        return imgAttr;
    }

    public void setImgAttr(String imgAttr) {
        this.imgAttr = imgAttr;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public int getLinkAvailability() {
        return linkAvailability;
    }

    public void setLinkAvailability(int linkAvailability) {
        this.linkAvailability = linkAvailability;
    }

    public ArrayList<UrlCategory> getUrlCategories() {
        return urlCategories;
    }

    public void setUrlCategories(ArrayList<UrlCategory> urlCategories) {
        this.urlCategories = urlCategories;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public String toString() {
        return "SiteDetail{" +
                "name='" + name + '\'' +
                ", urlCategories=" + urlCategories +
                ", baseUrl='" + baseUrl + '\'' +
                '}';
    }
}
