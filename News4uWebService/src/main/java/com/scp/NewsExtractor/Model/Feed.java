package com.scp.NewsExtractor.Model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * User: prashanth.v
 * Date: 2/17/13
 * Time: 8:36 PM
 */
public class Feed {

    private UrlCategory urlCategory;
    private String title;
    private String description;
    private String link;
    private Date date;
    private String contents;
    private String originalUrl;
    private ArrayList<String> category;
    private ArrayList<String> personRelatedKwds;
    private ArrayList<String> placeRelatedKwds;
    private ArrayList<String> orgRelatedKwds;
   // private String imageSrc;
    private ImageBean image;

    public Feed(UrlCategory urlCategory, String title, String description, String link, Date date, String contents, String originalUrl, ArrayList<String> category, ArrayList<String> personRelatedKwds, ArrayList<String> placeRelatedKwds, ArrayList<String> orgRelatedKwds, ImageBean image) {
        this.urlCategory = urlCategory;
        this.title = title;
        this.description = description;
        this.link = link;
        this.date = date;
        this.contents = contents;
        this.originalUrl = originalUrl;
        this.category = category;
        this.personRelatedKwds = personRelatedKwds;
        this.placeRelatedKwds = placeRelatedKwds;
        this.orgRelatedKwds = orgRelatedKwds;
        this.image = image;
    }

    public static final RowMapper<Feed> rowMapper = new RowMapper<Feed>() {
        @Override
        public Feed mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Feed(resultSet);
        }
    };

    public Feed(ResultSet resultSet) throws SQLException {
        ArrayList<String> categories = new ArrayList<String>();
        ArrayList<String> persons = new ArrayList<String>();
        ArrayList<String> places = new ArrayList<String>();
        ArrayList<String> orgs = new ArrayList<String>();
        persons.addAll(Arrays.asList(resultSet.getString("personRelatedKeywords").split(",")));
        places.addAll(Arrays.asList(resultSet.getString("placeRelatedKeywords").split(",")));
        orgs.addAll(Arrays.asList(resultSet.getString("orgRelatedKeywords").split(",")));
        categories.add(resultSet.getString("category"));
        this.urlCategory = new UrlCategory(resultSet.getString("referringUrl"), resultSet.getString("urlCategory"), 10);
        this.title = resultSet.getString("title");
        this.description = resultSet.getString("description");
        this.link = resultSet.getString("link");
        this.date = resultSet.getDate("date");
        this.contents = resultSet.getString("contents");
        this.originalUrl = resultSet.getString("originalUrl");
        this.category = categories;
        this.personRelatedKwds = persons;
        this.placeRelatedKwds = places;
        this.orgRelatedKwds = orgs;
        this.image = new ImageBean(resultSet.getString("imageSrc"),resultSet.getInt("width"),resultSet.getInt("height"));

    }

    public Feed(UrlCategory urlCategory, String title, String description, String link, Date date, ArrayList<String> category) {
        this.urlCategory = urlCategory;
        this.title = title;
        this.description = description;
        this.link = link;
        this.date = date;
        this.category = category;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public UrlCategory getUrlCategory() {
        return urlCategory;
    }

    public void setUrlCategory(String referringUrl) {
        this.urlCategory = urlCategory;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<String> category) {
        this.category = category;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ArrayList<String> getPersonRelatedKwds() {
        return personRelatedKwds;
    }

    public void setPersonRelatedKwds(ArrayList<String> personRelatedKwds) {
        this.personRelatedKwds = personRelatedKwds;
    }

    public ArrayList<String> getPlaceRelatedKwds() {
        return placeRelatedKwds;
    }

    public void setPlaceRelatedKwds(ArrayList<String> placeRelatedKwds) {
        this.placeRelatedKwds = placeRelatedKwds;
    }

    public ArrayList<String> getOrgRelatedKwds() {
        return orgRelatedKwds;
    }

    public void setOrgRelatedKwds(ArrayList<String> orgRelatedKwds) {
        this.orgRelatedKwds = orgRelatedKwds;
    }

    public ImageBean getImage() {
        return image;
    }

    public void setImage(ImageBean imageSrc) {
        this.image = imageSrc;
    }

    @Override
    public String toString() {
        return "Feed{\n" +
                "referringUrl='" + urlCategory + '\'' + "\n" +
                ", title='" + title + '\'' + "\n" +
                ", description='" + description + '\'' + "\n" +
                ", link='" + link + '\'' + "\n" +
                ", date=" + date + "\n" +
                ", originalUrl='" + originalUrl + '\'' + "\n" +
                ", category=" + toString(category) + "\n" +
                ", personRelatedKwds=" + toString(personRelatedKwds) + "\n" +
                ", placeRelatedKwds=" + toString(placeRelatedKwds) + "\n" +
                ", orgRelatedKwds=" + toString(orgRelatedKwds) + "\n" +
                ", imageSrc='" + image + '\'' + "\n" +
                '}';
    }

    public String toString(ArrayList<String> elements) {
        String result = "[";
        for (String x : elements) {
            result += " " + x + " , ";
        }
        return result.trim() + "]";
    }
}
