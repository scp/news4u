package com.scp.NewsExtractor.Controller;

import com.scp.NewsExtractor.Database.DBUtil;
import com.scp.NewsExtractor.Model.Feed;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


/**
 * Created by IntelliJ IDEA.
 * User: prashanth.v
 * Date: 2/13/13
 * Time: 2:05 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller

public class NewsController {
    private static Logger log=Logger.getLogger(NewsController.class);
    @RequestMapping(value="/")
    public String index(){
        return "tileLoader";
    }
    @RequestMapping(value="/login",method= RequestMethod.POST)
    @ResponseBody
    public String next(@RequestParam String uname,@RequestParam String pass){
        if(uname.equals("rachana") && pass.equals("sridhar")){
              return "Perfect Match ! Best ever pair!";
        }
        else
            return "Wrong Pair !!";
    }
    @RequestMapping(value="/category/{name}",method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Feed>  getFeedsForCategory(@PathVariable String name){
        ArrayList<Feed> feeds=null;
        try {
            feeds=DBUtil.getFeeds(name,10,1);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return feeds;
    }

}
