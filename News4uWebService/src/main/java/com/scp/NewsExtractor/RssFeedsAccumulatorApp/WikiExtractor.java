package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.scp.NewsExtractor.Bean.Constants;
import com.scp.NewsExtractor.util.Util;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * User: prashanth.v
 * Date: 2/23/13
 * Time: 7:53 PM
 */
public class WikiExtractor implements Callable<ArrayList<String>> {

    private static Logger log = Logger.getLogger(WikiExtractor.class);
    private JsonParser parser = new JsonParser();
    private String text;
    private int type;

    public WikiExtractor(String text, int type) {
        this.text = text;
        this.type = type;
    }


    public ArrayList<String> getTitlesAndCategories(String searchTerm) throws IOException {
        String categoryUrl = String.format("%squery&generator=search&gsrsearch=%s&prop=categories", Constants.WIKI_BASE_URL.getStringValue(), URLEncoder.encode(searchTerm, "UTF-8"));
        log.info("URL:"+categoryUrl);
        String categoryJsonResult = extractJsonResponseFromUrl(categoryUrl);
        if(categoryJsonResult==null || categoryJsonResult.equals("")) return null;
        Map<String, Object> result = getJsonPageResultForKey(categoryJsonResult, "title", true, "categories");
        log.info(result+" "+categoryUrl);
        if(result==null) return null;
        ArrayList<String> titles = (ArrayList<String>) result.get("title");
        ArrayList<JsonArray> categoryArrays = (ArrayList<JsonArray>) result.get("array");
        ArrayList<String> categories = new ArrayList<String>();
        final String word = searchTerm;
        for (JsonElement category : categoryArrays) {
            if (category != null) {
                JsonArray categoryArrayObj = category.getAsJsonArray();
                for (JsonElement element : categoryArrayObj) {
                    String categoryName = element.getAsJsonObject().get("title").getAsString();
                    if (!categoryName.toLowerCase().contains("article") && !categoryName.toLowerCase().contains("wikipedia") && !categoryName.toLowerCase().contains("births"))
                        categories.add(categoryName.replace("Category:", ""));
                }

            }

        }

        Collections.sort(titles, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int x = Util.LCS(o1, word);
                int y = Util.LCS(o2, word);
                if (x > y) return -1;
                else if (x == y) return 0;
                else return 1;
            }
        });

        log.info("ADDED TITLES FROM WIKI");
        titles.addAll(categories);

        return titles;
    }

    public ArrayList<String> imageExtractor(String searchTerm) {
        String imageUrl = String.format("%squery&generator=search&gsrsearch=%s&prop=images", Constants.WIKI_BASE_URL.getStringValue(), searchTerm);
        String finalUrl = "http://en.wikipedia.org/wiki/File:3rareiphoneviews.jpg";
        return null;
    }


    public String getExtract(String titles) throws IOException {
        String extractUrl = String.format("%squery&prop=extracts&titles=%s&exintro=1", Constants.WIKI_BASE_URL.getStringValue(), URLEncoder.encode(titles, "UTF-8"));
        String extractResult = extractJsonResponseFromUrl(extractUrl);
        Map<String, Object> result = getJsonPageResultForKey(extractResult, "extract", false);
        ArrayList<String> extracts = (ArrayList<String>) result.get("extract");
        // String extractInfor = "http://en.wikipedia.org/w/api.php?action=query&prop=revisions&titles=IPhone%204S&format=json&rvprop=content";
        return null;
    }

    private Map<String, Object> getJsonPageResultForKey(String jsonResponse, String key, boolean jsonArrayExpected, String... expectedArrayKeys) {
        JsonObject jsonObject = this.parser.parse(jsonResponse).getAsJsonObject().getAsJsonObject("query").getAsJsonObject("pages");
        Map<String, Object> result = new HashMap<String, Object>();
        ArrayList<String> resultKey = new ArrayList<String>();
        ArrayList<JsonArray> expectedArrayResult = new ArrayList<JsonArray>();
        if(jsonObject==null){log.info(jsonResponse+" Dint get Proper Page Object");  return null;}
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            JsonElement pages = entry.getValue();
            JsonObject page = pages.getAsJsonObject();
            resultKey.add(page.get(key).getAsString());
            if (jsonArrayExpected) {
                for (String arrayKey : expectedArrayKeys) {
                    expectedArrayResult.add(page.getAsJsonArray(arrayKey));
                }
            } else
                expectedArrayResult = null;
        }
        result.put(key, resultKey);
        result.put("array", expectedArrayResult);
        return result;
    }

    public String extractJsonResponseFromUrl(String wikiUrl) throws IOException {
        BufferedReader br = null;
        HttpURLConnection connection = null;
        StringBuilder resultJson = new StringBuilder();
        try {
            URL url = new URL(wikiUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            if (connection.getResponseCode() != 200) {
                log.error("Failed : HTTP error code : " + connection.getResponseCode());
            }
            br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
            String output;
            while ((output = br.readLine()) != null) {
                resultJson.append((output));
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (br != null)
                br.close();
            if (connection != null)
                connection.disconnect();

        }
        return resultJson.toString();
    }

    public static void main(String[] args) throws IOException {

    }

    public ArrayList<String> getPlacesRelatedTopics(String place) throws IOException {
        String topicExtractUrl = String.format("%sopensearch&search=%s&limit=100", Constants.WIKI_BASE_URL.getStringValue(),URLEncoder.encode(place,"UTF-8"));
        String topicExtractJsonResult = extractJsonResponseFromUrl(topicExtractUrl);
        if(topicExtractJsonResult==null || topicExtractJsonResult.equals("")) return null;
        JsonArray placeArray = this.parser.parse(topicExtractJsonResult).getAsJsonArray();
        ArrayList<String> relatedTopics = new ArrayList<String>();
        if (placeArray.size() >= 2) {
            JsonElement placeJsonElement = placeArray.get(1);
            JsonArray placeRelatedTopics = placeJsonElement.getAsJsonArray();
            for (JsonElement placeTopic : placeRelatedTopics) {
                relatedTopics.add(placeTopic.getAsString());
            }
        }
        return relatedTopics;

    }

    public ArrayList<String> getNameOrgRelatedTopics(String name) throws IOException {
        return getTitlesAndCategories(name);
    }

    @Override
    public ArrayList<String> call() throws Exception {
        if (type == Constants.PLACE_TYPE.getNumericValue())
            return getPlacesRelatedTopics(text);
        else //if(type==Constants.ORG_TYPE.getNumericValue() || type==Constants.NAME_TYPE.getNumericValue())
            return getNameOrgRelatedTopics(text);

    }

}
