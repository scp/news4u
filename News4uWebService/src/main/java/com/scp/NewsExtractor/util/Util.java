package com.scp.NewsExtractor.util;

import com.scp.NewsExtractor.Bean.Constants;
import org.apache.log4j.Logger;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:23 PM
 */
public class Util {
    private static Logger log = Logger.getLogger(Util.class);
    public static int LCS(String X, String Y) {
        final int MAX = 505;
        int[][] L;
        int[][] D;

        L = new int[X.length() + 2][Y.length() + 2];
        D = new int[X.length() + 2][Y.length() + 2];
        int m = X.length();
        int n = Y.length();

        for (int i = 1; i <= m; i++) L[i][0] = 0;
        for (int j = 0; j <= n; j++) L[0][j] = 0;

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (X.charAt(i - 1) == Y.charAt(j - 1)) {
                    L[i][j] = L[i - 1][j - 1] + 1;
                    D[i][j] = 1;
                } else if (L[i - 1][j] >= L[i][j - 1]) {
                    L[i][j] = L[i - 1][j];
                    D[i][j] = 2;
                } else {
                    L[i][j] = L[i][j - 1];
                    D[i][j] = 3;
                }
            }
        }
        return L[m][n];
    }

    public static int getKeywordType(String key) {
        if (key.contains("loc"))
            return Constants.PLACE_TYPE.getNumericValue();
        else if (key.contains("org"))
            return Constants.ORG_TYPE.getNumericValue();
        else if (key.contains("name"))
            return Constants.NAME_TYPE.getNumericValue();
        else
            return Constants.MISC_TYPE.getNumericValue();
    }


    public static String isStringSet(String str) {

        if (str == null || str.trim().length() == 0 || str.trim().equalsIgnoreCase("null")) {
            return null;
        } else {
            return str.trim();
        }
    }
}
