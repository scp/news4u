package com.scp.NewsExtractor.Database;

import com.scp.NewsExtractor.Model.Feed;
import com.scp.NewsExtractor.Model.ImageBean;
import com.scp.NewsExtractor.Model.UrlCategory;
import com.scp.NewsExtractor.util.ApplicationProperties;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * User: prashanth.v
 * Date: 2/28/13
 * Time: 4:06 PM
 */
public class DBUtil {
    private static final Logger log = Logger.getLogger(DBUtil.class);
    private static Random random = new Random();


    public static Database getDB() {
        try {
            return new Database(ApplicationProperties.getProperty("DB_MYSQL_DRIVER"),
                    ApplicationProperties.getProperty("DB_URL"),
                    ApplicationProperties.getProperty("DB_USER"),
                    ApplicationProperties.getProperty("DB_PASSWORD"),
                    Boolean.parseBoolean(ApplicationProperties.getProperty("DB_AUTO_COMMIT")));
        } catch (Exception e) {
            throw new RuntimeException("Cannot connect to DB", e);
        }
    }

    public static ArrayList<Feed> getFeeds(String category, int count, int lastId) throws Exception {
        ArrayList<Feed> feeds = new ArrayList<Feed>();
        Database database = null;
        ResultSet resultSet = null;

        try {
            database = getDB();
            String query = "SELECT * FROM feeds where id > ? and urlCategory like ? LIMIT ? ";
            PreparedStatement getFeedsStmt = database.getPreparedStatement(query);
            getFeedsStmt.setInt(1, lastId);
            getFeedsStmt.setString(2, "%" + category + "%");
            getFeedsStmt.setInt(3, count);
            resultSet = getFeedsStmt.executeQuery();
            while (resultSet.next()) {
                ArrayList<String> categories = new ArrayList<String>();
                ArrayList<String> persons = new ArrayList<String>();
                ArrayList<String> places = new ArrayList<String>();
                ArrayList<String> orgs = new ArrayList<String>();
                persons.addAll(Arrays.asList(resultSet.getString("personRelatedKeywords").split(",")));
                places.addAll(Arrays.asList(resultSet.getString("placeRelatedKeywords").split(",")));
                orgs.addAll(Arrays.asList(resultSet.getString("orgRelatedKeywords").split(",")));
                String src = resultSet.getString("imageSrc");
                int width=resultSet.getInt("width");
                int height=resultSet.getInt("height");
                if (src == null || src.equals("null") || src.length() < 2) {
                    src = "/static/images/" + resultSet.getString("urlCategory") + "/" + random.nextInt(7) + ".jpg";
                     width = 100;
                     height = 100;
                } else {
                    if (!src.startsWith("http://")) {
                        src = "http://www.ndtv.com/" + src;
                    }
                    if(width==-1 || height==-1){
                        width=200;
                        height=200;
                    }
                }
                categories.add(resultSet.getString("category"));
                System.out.println(resultSet.getString("title"));
                feeds.add(new Feed(new UrlCategory(resultSet.getString("referringUrl"), resultSet.getString("urlCategory"), 10),
                        resultSet.getString("title"), resultSet.getString("description"), resultSet.getString("link"),
                        resultSet.getDate("date"), resultSet.getString("contents"), resultSet.getString("originalUrl"),
                        categories, persons, places, orgs, new ImageBean(src,width,height) ));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (database != null) {
                database.close();
            }
        }
        return feeds;

    }

    public static void main(String[] args) throws Exception {
       getFeeds("Top Stories",10,1);
    }

}
