package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Constants;
import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.Bean.UrlCategory;
import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 8:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProcessingQueues {

    private static Logger log = Logger.getLogger(ProcessingQueues.class);
    private static final LinkedBlockingQueue<Feed> feedLearningQueue = new LinkedBlockingQueue<Feed>();
    private static final LinkedBlockingQueue<SiteDetail> urlToFeedExtractorQueue = new LinkedBlockingQueue<SiteDetail>();
    private static final LinkedBlockingQueue<UrlCategory> siteToBeProcessedQueue = new LinkedBlockingQueue<UrlCategory>();
    private static ExecutorService feedExtractorExcecutorService = Executors.newFixedThreadPool(Constants.DOWNLOAD_THREAD_COUNT.getNumericValue());
    private static ExecutorService contentExtractorExcecutorService = Executors.newFixedThreadPool(Constants.DOWNLOAD_THREAD_COUNT.getNumericValue());
    private static ExecutorService keywordExtractorExecutorService = Executors.newFixedThreadPool(Constants.DOWNLOAD_THREAD_COUNT.getNumericValue());
    private static ExecutorService wikiExtractorExecutorService = Executors.newFixedThreadPool(Constants.DOWNLOAD_THREAD_COUNT.getNumericValue());
    private static ExecutorService entityExtractorExecutorService = Executors.newFixedThreadPool(Constants.DOWNLOAD_THREAD_COUNT.getNumericValue());
    private static volatile ProcessingQueues instance;
    private static int size;


    public static ProcessingQueues getInstance() {
        if (instance == null) {
            synchronized (ProcessingQueues.class) {
                if (instance == null)
                    instance = new ProcessingQueues();
            }
        }
        return instance;
    }

    public void enqueueForFeedLearningQueue(Feed feed) throws InterruptedException {
        feedLearningQueue.put(feed);
    }

    public Feed dequeueForFeedLearningQueue() throws InterruptedException {
        return feedLearningQueue.poll();
    }

    public void enqueueForFeedExtraction(SiteDetail site) throws InterruptedException {
        urlToFeedExtractorQueue.put(site);
    }

    public SiteDetail dequeueForFeedExtraction() throws InterruptedException {
        SiteDetail siteDetail = urlToFeedExtractorQueue.take();
        enqueueForFeedExtraction(siteDetail);
        return siteDetail;
    }

    public void enqueueSitesProcessing(UrlCategory site) throws InterruptedException {
        siteToBeProcessedQueue.put(site);
    }

    public UrlCategory dequeueForSitesProcessing() throws InterruptedException {
        return siteToBeProcessedQueue.take();
    }


    public static ExecutorService getContentExtractorExcecutorService() {
        return contentExtractorExcecutorService;
    }

    public static ExecutorService getFeedExtractorExcecutorService() {
        return feedExtractorExcecutorService;
    }

    public static ExecutorService getkeywordExtractorExcecutorService() {
        return keywordExtractorExecutorService;
    }

    public static ExecutorService getWikiExtractorExcecutorService() {
        return wikiExtractorExecutorService;
    }

    public static ExecutorService getEntityExtractorExcecutorService() {
        return entityExtractorExecutorService;
    }

    public static int getSize() {
        return feedLearningQueue.size();
    }

    public static void setSize(int size) {
        ProcessingQueues.size = size;
    }
}
