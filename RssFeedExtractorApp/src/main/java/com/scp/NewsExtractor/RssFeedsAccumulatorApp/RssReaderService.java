package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.Bean.UrlCategory;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * User: prashanth.v
 * Date: 2/17/13
 * Time: 9:15 PM
 */
public class RssReaderService implements Runnable {

    private static Logger log = Logger.getLogger(RssReaderService.class);
    private int size, expectedFeeds;

    public RssReaderService(int size, int expectedFeeds) {
        this.size = size;
        this.expectedFeeds = expectedFeeds;
    }

    @Override
    public void run() {
        System.out.println("THUU LOSEEEE"+size+" "+expectedFeeds);
        try {
            int noOfSites=size;
            CountDownLatch latch = new CountDownLatch(expectedFeeds);
            int sum = 0;
            if (noOfSites > 0) {
                while (noOfSites > 0) {
                    SiteDetail siteDetail = ProcessingQueues.getInstance().dequeueForFeedExtraction();
                    ArrayList<UrlCategory> urlCategories = siteDetail.getUrlCategories();
                    for (UrlCategory urlCategory : urlCategories) {
                        sum += urlCategory.getLimit();
                        ProcessingQueues.getFeedExtractorExcecutorService().submit(new RssReader(urlCategory, siteDetail, latch));
                    }
                    noOfSites--;
                }
                log.info("EXPECTED THREADS:" + sum);
                latch.await(4, TimeUnit.MINUTES);
                log.info("Size:" + ProcessingQueues.getSize());
              /*  FeedDataIndexer indexer = new FeedDataIndexer();
                indexer.indexFeedData(ProcessingQueues.getSize());
                while (ProcessingQueues.getSize() > 0) {
                    Feed feed = ProcessingQueues.getInstance().dequeueForFeedLearningQueue();
                    log.info(feed.toString());
                    indexer.findSimilarDocs(feed);
                    indexer.searchSimilarDocs(feed);
                }
                System.out.println("Done nowww");*/
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

    }
}
