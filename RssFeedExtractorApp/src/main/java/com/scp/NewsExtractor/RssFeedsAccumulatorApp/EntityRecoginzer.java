package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Constants;
import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.util.Util;
import edu.stanford.nlp.util.Triple;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: prashanth.v
 * Date: 2/22/13
 * Time: 8:03 PM
 */
public class EntityRecoginzer implements Runnable {

    private static Logger log = Logger.getLogger(EntityRecoginzer.class);
    private Feed feed;
    private CountDownLatch latch;

    public EntityRecoginzer() {
    }

    public EntityRecoginzer(Feed feed, CountDownLatch latch) {
        this.feed = feed;
        this.latch = latch;
    }

    public static void main(String[] args) throws IOException {

        String s1 = "Good afternoon Rajat Raina, how are you today? cancer";
        String s2 = s1 + " I go to school at Stanford University, which is located in California and i have cancer.";
        //System.out.println(classifier.classifyToString(s2));
        EntityRecoginzer ner = new EntityRecoginzer();


    }

    public Map<String, ArrayList<String>> getEntities(Feed feed) {

        Map<String, ArrayList<String>> keywordList = new HashMap<String, ArrayList<String>>();
        String annotatedList = Constants.CLASSIFIER.getClassifier().classifyWithInlineXML(feed.getContents());
        Pattern pattern = Pattern.compile("(<.*?>)(.+?)(</.*?>)");
        ArrayList<String> name = new ArrayList<String>();
        ArrayList<String> loc = new ArrayList<String>();
        ArrayList<String> org = new ArrayList<String>();
        ArrayList<String> misc = new ArrayList<String>();
        Matcher matcher = pattern.matcher(annotatedList);
        while (matcher.find()) {
            String entity = matcher.group(2);
            String entityName = matcher.group(1);
            if (entityName.toLowerCase().contains("loc")) {
                loc.add(entity);
            } else if (entityName.toLowerCase().contains("org")) {
                org.add(entity);
            } else if (entityName.toLowerCase().contains("person")) {
                name.add(entity);
            } else {
                misc.add(entity);
            }

        }
        keywordList.put("loc", loc);
        keywordList.put("org", org);
        keywordList.put("name", name);
        keywordList.put("misc", misc);
        log.info("Entity Recognizer for feed from:"+feed.getUrlCategory().getUrl()+" "+latch.getCount());
        return keywordList;

    }
    public Map<String, ArrayList<String>> getNamedEntities(Feed feed) {
            Map<String, ArrayList<String>> keywordList = new HashMap<String, ArrayList<String>>();
            ArrayList<String> name = new ArrayList<String>();
            ArrayList<String> loc = new ArrayList<String>();
            ArrayList<String> org = new ArrayList<String>();
            ArrayList<String> misc = new ArrayList<String>();
            String contents = feed.getTitle() + " " + feed.getContents();
            List triples = Constants.CLASSIFIER.getClassifier().classifyToCharacterOffsets(contents);
            for (Triple triple : (List<Triple>) triples) {
                String entity = contents.substring((Integer) triple.second, (Integer) triple.third);
                String entityName = (String) triple.first;
                if(entity.toLowerCase().contains("india"))
                    continue;
                if (entityName.toLowerCase().contains("loc")) {
                    loc.add(entity);
                } else if (entityName.toLowerCase().contains("org")) {
                    org.add(entity);
                } else if (entityName.toLowerCase().contains("person")) {
                    name.add(entity);
                } else {
                    misc.add(entity);
                }
            }
            keywordList.put("loc", loc);
            keywordList.put("org", org);
            keywordList.put("name", name);
            keywordList.put("misc", misc);
            return keywordList;
        }


    @Override
    public void run() {
        try {
        Map<String, ArrayList<String>> entities = getNamedEntities(feed);
        log.info("Entity:" + entities + " " + latch.getCount());

            for (Map.Entry<String, ArrayList<String>> entry : entities.entrySet()) {
                Future<ArrayList<String>> keywordFutureList = ProcessingQueues.getkeywordExtractorExcecutorService().submit(new KeywordsExtractor(entry));
                if (keywordFutureList != null && keywordFutureList.get() != null) {
                    if (Util.getKeywordType(entry.getKey()) == Constants.PLACE_TYPE.getNumericValue()) {
                        feed.setPlaceRelatedKwds(keywordFutureList.get());
                    } else if (Util.getKeywordType(entry.getKey()) == Constants.NAME_TYPE.getNumericValue()) {
                        feed.setPersonRelatedKwds(keywordFutureList.get());
                    } else {
                        feed.setOrgRelatedKwds(keywordFutureList.get());
                    }
                }
            }

            //latch.countDown();
            ProcessingQueues.getInstance().enqueueForFeedLearningQueue(feed);

        } catch (Exception e) {
            log.info("DINT MAKE A COUNTDOWN");
            log.error(e.getMessage(), e);
        }
        latch.countDown();
        System.out.println("Finally:" + latch.getCount()+" size="+ProcessingQueues.getSize());

    }
}
