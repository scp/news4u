package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.Bean.Image;
import com.scp.NewsExtractor.Bean.ImageBean;
import com.scp.NewsExtractor.Bean.SiteDetail;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * User: prashanth.v
 * Date: 2/25/13
 * Time: 4:13 PM
 */
public class ImgExtractor {
    private static Logger log = Logger.getLogger(ImgExtractor.class);

    public void extractImgSrc(Feed feed, SiteDetail siteDetail) throws Exception {
        try {
            Elements imgTags = null;
            Document doc = Jsoup.connect(feed.getOriginalUrl()).get();
            String imgSrc = null;
            if (siteDetail.getImgAttr().equalsIgnoreCase("found")) {
                Document descDoc = Jsoup.parse(feed.getDescription());
                imgTags = descDoc.getElementsByTag("img");
            } else {
                imgTags = doc.getElementsByAttributeValue(siteDetail.getImgAttr(), siteDetail.getVal());
            }
            for (Element img : imgTags) {
                imgSrc = img.attr("src");
                String src = img.attr("src").toLowerCase();
                if (src.endsWith(".jpg") || src.endsWith(".jpeg") || src.endsWith(".png"))
                    break;
                else {
                    imgSrc = null;
                }
            }

            feed.setImage(new ImageBean(imgSrc));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            feed.setImage(null);
        }
        if (feed.getImage() == null || feed.getImage().getImageSrc() == null || feed.getImage().getImageSrc().length() > 2)
            getImagesBP(feed);
        if (feed.getImage() != null) {
            String src = "";
            if ((src = feed.getImage().getImageSrc()) != null) {
                if (src.length() > 0) {
                    if (!src.trim().startsWith("http")) {
                        URI uri = new URI(feed.getOriginalUrl());
                        src = "http://" + uri.getHost() + "/" + src;
                    }
                    getSize(src, feed.getImage());
                }
            }
        }
        log.info("IMAGEEEE:" + feed.getImage() + "  \nLink:" + feed.getOriginalUrl());
    }

    public void getSize(final String url, final ImageBean image) {
        new SwingWorker<BufferedImage, Void>() {
            @Override
            protected BufferedImage doInBackground() throws Exception {
                log.info("Image url" + url);
                return ImageIO.read(new URL(url));
            }

            @Override
            protected void done() {
                try {
                    BufferedImage img = get();
                    if (img != null) {
                        image.setHeight(img.getHeight());
                        image.setWidth(img.getWidth());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    public static void main(String[] args) throws URISyntaxException {
        URI uri = new URI("http://sports.ndtv.com/kjshdkjf/sakjdka");
        System.out.println(uri.getHost());
    }


    public void getImagesBP(Feed feed) {
        try {
            log.info(feed.getOriginalUrl());
            URL url = new URL(feed.getOriginalUrl());
            log.info("TRYING BOILER PIPE IMAGE EXTRACTION");
            final BoilerpipeExtractor extractor = CommonExtractors.ARTICLE_EXTRACTOR;
            final ImageExtractor ie = ImageExtractor.INSTANCE;
            List<Image> imgUrls = ie.process(url, extractor);
            Collections.sort(imgUrls);
            for (Image img : imgUrls) {
                log.info("*URL::" + img.getSrc());
                feed.setImage(new ImageBean(img.getSrc()));
                break;
            }

        } catch (Exception e) {
            feed.setImage(null);
            log.error(e.getMessage(), e);
        }
    }
}
