package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.util.Util;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 2/24/13
 * Time: 11:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeywordsExtractor implements Callable<ArrayList<String>> {

    private Map.Entry<String, ArrayList<String>> entry;

    public KeywordsExtractor(Map.Entry<String, ArrayList<String>> entry) {
        this.entry = entry;
    }

    @Override
    public ArrayList<String> call() throws Exception {

        ArrayList<String> keywordsForType = new ArrayList<String>();
        try{
        int type = Util.getKeywordType(entry.getKey());
        for (String entities : entry.getValue()) {
            Future<ArrayList<String>> kwdType = ProcessingQueues.getWikiExtractorExcecutorService().submit(new WikiExtractor(entities, type));
            if (kwdType!=null && kwdType.get() != null) {
                keywordsForType.addAll(kwdType.get());
            }
        }
        }catch (Exception e){
            e.printStackTrace();
        }
        return keywordsForType;

    }
}
