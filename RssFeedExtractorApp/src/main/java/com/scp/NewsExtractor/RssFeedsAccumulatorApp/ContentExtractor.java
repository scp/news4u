package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.Bean.UrlCategory;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.CountDownLatch;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 11:15 PM
 */
public class ContentExtractor implements Runnable {

    private static Logger log = Logger.getLogger(ContentExtractor.class);
    private Feed feed;
    private UrlCategory urlCategory;
    private CountDownLatch latch;
    private SiteDetail siteDetail;

    public ContentExtractor(Feed feed, UrlCategory urlCategory, SiteDetail siteDetail, CountDownLatch latch) {
        this.feed = feed;
        this.urlCategory = urlCategory;
        this.latch = latch;
        this.siteDetail = siteDetail;
    }

    public String extractTextUsingCleanUp(Feed feed) throws IOException {
        URL url = null;
        String text = null;
        InputStream is = null;
        try {
            url = new URL(feed.getLink());
            if (siteDetail.getLinkAvailability() == 0) {
                URLConnection connection = url.openConnection();
                connection.connect();
                //connection.wait(10000);
                is = connection.getInputStream();
                URL origUrl = connection.getURL();
                if (origUrl != null)
                    feed.setOriginalUrl(origUrl.toString());
                else
                    feed.setOriginalUrl(feed.getLink());
            } else {
                feed.setOriginalUrl(feed.getLink());
            }
            log.info("LINK:" + feed.getLink());
            text = ArticleExtractor.INSTANCE.getText(url);
        } catch (Exception e) {
            log.info("DOG:" + feed.getTitle());
            log.error(e.getMessage(), e);
        } finally {
            if (is != null)
                is.close();
        }
        return text;
    }

    public static void main(String[] args) {
    }


    @Override
    public void run() {
        String text = null;
        try {
            log.info("Content Extraction Entry:");
            text = extractTextUsingCleanUp(feed);
            log.info("IPO:" + feed.getTitle());
            if (text == null) {
                text = feed.getDescription();
                System.out.println("Not found!!" + feed.getTitle());
            }
            ImgExtractor imgExtractor = new ImgExtractor();
            try {
                imgExtractor.extractImgSrc(feed, siteDetail);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            text = text.replaceAll("<.*?>", " ").trim();
            log.info("Content Extraction Done along with Image:");
            feed.setContents(text);
            log.info("Entity Recognition Scheduling Threads:");
            ProcessingQueues.getEntityExtractorExcecutorService().submit(new EntityRecoginzer(feed, latch));
            System.out.println(latch.getCount());
        } catch (Exception e) {
            log.error("INGAYE PRACHNA LOOSE");
            log.error(e.getMessage(), e);
        }
    }

    private void process(Feed feed) {
        System.out.println("Content" + feed.getTitle() + "\n" + feed.getDescription());
    }

}
