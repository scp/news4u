package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.Bean.UrlCategory;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 11:08 PM
 */
public class RssReader implements Runnable {

    private static Logger log = Logger.getLogger(RssReader.class);
    private UrlCategory urlCategory;
    private String name, baseUrl;
    private CountDownLatch latch;
    private SiteDetail siteDetail;


    public RssReader(UrlCategory urlCategory, SiteDetail siteDetail, CountDownLatch latch) {
        this.urlCategory = urlCategory;
        this.siteDetail=siteDetail;
        this.latch = latch;
    }

    public void getContentExtractedRssFeeds(UrlCategory urlCategory) throws IOException {
        String rssUrl=urlCategory.getUrl();
        int limit=urlCategory.getLimit();
        URL url = new URL(rssUrl);
        XmlReader reader = null;
        ArrayList<Feed> feeds = new ArrayList<Feed>();
        try {
            reader = new XmlReader(url);
            SyndFeed feed = new SyndFeedInput().build(reader);
            int i = 0;
            for (Object o : feed.getEntries()) {
                if (i >= limit) break;
                SyndEntry entry = (SyndEntry) o;
                ArrayList<String> category = new ArrayList<String>();
                for (SyndCategoryImpl cat : (List<SyndCategoryImpl>) entry.getCategories()) {
                    category.add(cat.getName());
                }

                Feed newFeed = new Feed(urlCategory, entry.getTitle(), entry.getDescription().getValue(), entry.getLink(),
                        entry.getPublishedDate(), category);
                feeds.add(newFeed);
                log.info("ADDING FEEDS TO EXTRACT CONTENT:"+latch.getCount()+" :for"+feed.getLink());
                addFeedToQueue(newFeed);
                i++;
            }

        } catch (FeedException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (reader != null)
                reader.close();
        }

    }

    private void addFeedToQueue(Feed newFeed) {
        ProcessingQueues.getContentExtractorExcecutorService().submit(new ContentExtractor(newFeed, urlCategory, siteDetail, latch));
    }

    @Override
    public void run() {
        try {
            getContentExtractedRssFeeds(urlCategory);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }


    }
}
