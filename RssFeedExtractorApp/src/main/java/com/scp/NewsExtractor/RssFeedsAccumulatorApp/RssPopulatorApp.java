package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Constants;
import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.util.Util;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 10:07 PM
 */
//@Component("RssFeedsWorker")
public class RssPopulatorApp {

    private static final Logger log = Logger.getLogger(RssPopulatorApp.class);
    static ScheduledExecutorService feedCollectingScheduler;
    private static int siteCount;
    private static int expectedFeeds;

    public static void main(String args[]) {
        fillSiteInfoList();
        startExtraction();

    }


    private static void fillSiteInfoList() {
        log.info("Reading site.xml");
        Object[] resultDetails = Util.readSiteXml(Constants.SITE_LINKS.getStringValue());
        ArrayList<SiteDetail> siteDetails = (ArrayList<SiteDetail>) resultDetails[0];
        expectedFeeds = (Integer) resultDetails[1];
        if (siteDetails != null)
            siteCount = siteDetails.size();
        else siteCount = 0;
    }

    private static void startExtraction() {
        //TODO0
        feedCollectingScheduler = Executors.newScheduledThreadPool(1);
        log.info("Total Sites:" + siteCount);
        feedCollectingScheduler.scheduleAtFixedRate(new RssReaderService(siteCount, expectedFeeds),0,2, TimeUnit.HOURS);
        //feedCollectingScheduler.scheduleAtFixedRate(new hello(),0,3, TimeUnit.SECONDS);

    }


}
