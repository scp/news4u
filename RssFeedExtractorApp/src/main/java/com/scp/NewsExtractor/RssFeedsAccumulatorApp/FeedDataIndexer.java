package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import com.scp.NewsExtractor.Bean.Feed;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queries.mlt.MoreLikeThisQuery;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * User: prashanth.v
 * Date: 2/19/13
 * Time: 6:59 PM
 */
public class FeedDataIndexer {

    private static Logger log = Logger.getLogger(FeedDataIndexer.class);
    private Directory indexDir;

    public FeedDataIndexer() {
        try {
            String currentDir=new File(".").getAbsolutePath();
            currentDir=currentDir.substring(0,currentDir.length()-1)+"index"+File.separator;
            File currentDirFile=new File(currentDir);
            if(!currentDirFile.exists()){
                currentDirFile.mkdirs();
            }
            indexDir = new SimpleFSDirectory(currentDirFile);//Constants.INDEX_PATH.getStringValue()));
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }

    public IndexWriter openIndex() {
        IndexWriter writer = null;
        try {
            EnglishAnalyzer analyzer = new EnglishAnalyzer(Version.LUCENE_41);//, new CharArraySet(Version.LUCENE_41, Constants.STOP_LIST.getStringList(), true));
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_41, analyzer);
            config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            writer = new IndexWriter(indexDir, config);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return writer;
    }

    public void indexFeedData(int expectedFeedCount) throws Exception {
        IndexWriter writer = openIndex();
        Feed feed;
        int processedFeedCount = 0;
        while ((feed = ProcessingQueues.getInstance().dequeueForFeedLearningQueue()) != null && processedFeedCount < expectedFeedCount) {
            ProcessingQueues.getInstance().enqueueForFeedLearningQueue(feed);
            System.out.println("Here" + processedFeedCount);
            addDoc(writer, feed);
            processedFeedCount++;
        }
        writer.close();
    }

    private static void addDoc(IndexWriter w, Feed feed) throws IOException {
        Document doc = new Document();
        doc.add(new StringField("link", feed.getLink(), Field.Store.YES));
        doc.add(new TextField("description", feed.getDescription(), Field.Store.YES));
        doc.add(new StringField("referringUrl", feed.getUrlCategory().getUrl(), Field.Store.YES));
        doc.add(new StringField("urlCategory", feed.getUrlCategory().getCategory(), Field.Store.YES));
        doc.add(new TextField("title", "" + feed.getTitle(), Field.Store.YES));
        doc.add(new LongField("date", feed.getDate().getTime(), Field.Store.YES));
        doc.add(new StringField("originalLink", feed.getOriginalUrl(), Field.Store.YES));
        String fullSearchtext = feed.getDescription() + "  " + feed.getTitle() + " " + " "+feed.getContents().substring(0,feed.getContents().length()/3);
        doc.add(new TextField("searchData", fullSearchtext, Field.Store.YES));
        Term term = new Term("originalLink", feed.getOriginalUrl());
        System.out.println("Updating docs for indexing");
        w.updateDocument(term, doc);
        //w.addDocument(doc);
    }

    public void findSimilarDocs(Feed feed) throws IOException {
        IndexReader ir = DirectoryReader.open(indexDir);
        MoreLikeThis mlt = new MoreLikeThis(ir);
        IndexSearcher searcher = new IndexSearcher(ir);
        System.out.println("No.Of docs:"+ir.numDocs());
        mlt.setMinTermFreq(1);
        mlt.setAnalyzer(new EnglishAnalyzer(Version.LUCENE_41));
        mlt.setMinDocFreq(1);
        String searchQuery=feed.getTitle()+" "+feed.getContents().substring(0,feed.getContents().length()/4);
        MoreLikeThisQuery query=new MoreLikeThisQuery(searchQuery,new String[]{"searchData"},new EnglishAnalyzer(Version.LUCENE_41),"title");
        TopDocs topDocs = searcher.search(query, 5);
        log.info("found " + topDocs.totalHits + " topDocs");
        System.out.println("title:" + feed.getTitle());
        List<String> foundQuotes = new ArrayList<String>();
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = searcher.doc(scoreDoc.doc);
            String text = doc.get("title");
            System.out.println("hello: " + text);
            foundQuotes.add(text);
        }

    }

    public ArrayList<String> searchSimilarDocs(Feed feed) throws IOException, ParseException {
        int hitsPerPage = 5;
        String contents=feed.getTitle()+" "+feed.getDescription();
        String queryKeyword=contents.replaceAll("[^a-zA-Z0-9\\s]"," ").trim();
        EnglishAnalyzer analyzer = new EnglishAnalyzer(Version.LUCENE_41);//, new CharArraySet(Version.LUCENE_41, Constants.STOP_LIST.getStringList(), true));
        ArrayList<String> urls = new ArrayList<String>();
        IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(indexDir));
        Query q = new QueryParser(Version.LUCENE_41, "searchData", analyzer).parse(queryKeyword);
        TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
        searcher.search(q, collector);

        ScoreDoc[] hits = collector.topDocs().scoreDocs;
        System.out.println("Found " + hits.length + " hits.");
        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            System.out.println((i + 1) + ". " + d.get("title") + "\t" + d.get("description") );
            urls.add(d.get("url"));

        }
        return urls;
    }

    private void deleteAll() throws Exception {
        IndexWriter writer = openIndex();
        writer.deleteAll();
    }


}