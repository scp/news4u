package com.scp.NewsExtractor.RssFeedsAccumulatorApp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: prashanth.v
 * Date: 2/24/13
 * Time: 3:19 AM
 */
public class AskExtractor {

    public static void crawlAsk(String searchTerm) throws IOException {
        Pattern patter= Pattern.compile("(\\(')(.*?)('\\))");
        StringBuilder latestRelatedNews=new StringBuilder();
        String url=String.format("http://www.ask.com/web?q=%s", URLEncoder.encode(searchTerm,"UTF-8"));
        ArrayList<String> news=new ArrayList<String>();
        Document doc = Jsoup.connect(url).timeout(10000).get();
        Elements yNews=doc.getElementsContainingText("News for");
        Elements topNews=(yNews.last().siblingElements());
        for(Element newsNode:topNews){
            Elements ahrefs=(newsNode.getElementsByTag("a"));
            for(Element a:ahrefs){
                String link=a.attr("onmouseover");
                Matcher matcher=patter.matcher(link);
                while(matcher.find())
                    if(a.hasText() && !a.text().equalsIgnoreCase("source"))
                    latestRelatedNews.append("<a href='").append(matcher.group(2)).append("'>").append(a.text()).append("</a>\n") ;
            }

        }
        System.out.println(latestRelatedNews.toString());
    }

    public static void main(String[] args) throws IOException {
         crawlAsk("Bombay news");

    }
}
