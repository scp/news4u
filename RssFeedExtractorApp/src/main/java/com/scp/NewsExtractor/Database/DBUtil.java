package com.scp.NewsExtractor.Database;

import com.scp.NewsExtractor.Bean.Feed;
import com.scp.NewsExtractor.Bean.ImageBean;
import com.scp.NewsExtractor.Bean.UrlCategory;
import com.scp.NewsExtractor.util.ApplicationProperties;
import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 * User: prashanth.v
 * Date: 2/28/13
 * Time: 4:06 PM
 */
public class DBUtil {
    private static final Logger log = Logger.getLogger(DBUtil.class);

    public static Database getDB() {
        try {
            return new Database(ApplicationProperties.getProperty("DB_MYSQL_DRIVER"),
                    ApplicationProperties.getProperty("DB_URL"),
                    ApplicationProperties.getProperty("DB_USER"),
                    ApplicationProperties.getProperty("DB_PASSWORD"),
                    Boolean.parseBoolean(ApplicationProperties.getProperty("DB_AUTO_COMMIT")));
        } catch (Exception e) {
            throw new RuntimeException("Cannot connect to DB", e);
        }
    }

    public static Map<String, Object> addFeeds(ArrayList<Feed> feeds) throws Exception {
        Database database = null;
        ResultSet resultSet = null;
        HashMap<String, Object> map = new HashMap<String, Object>();
        String xml = getAddFeedsXml(feeds);
        try {
            database = getDB();
            CallableStatement addOrigImgStmt = database.getCallableStatement("{ call Add_Feeds(?) }");
            addOrigImgStmt.setString(1, xml);
            resultSet = addOrigImgStmt.executeQuery();
            if (resultSet != null && resultSet.next()) {
                map = new HashMap<String, Object>();
                map.put("start", resultSet.getInt("begin"));
                map.put("end", resultSet.getString("end"));
                return map;
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (database != null) {
                database.close();
            }
        }
        return map;
    }


    public static String getAddFeedsXml(ArrayList<Feed> feeds) throws Exception {
        int i = 1;
        StringBuilder xml = new StringBuilder();
        xml.append("<ROOT>");
        for (Feed feed : feeds) {
            xml.append("<ROW>");
            xml.append("<REFERRING_URL><![CDATA[").append(feed.getUrlCategory().getUrl()).append("]]></REFERRING_URL>");
            xml.append("<URL_CATEGORY><![CDATA[").append(feed.getUrlCategory().getCategory()).append("]]></URL_CATEGORY>");
            xml.append("<TITLE><![CDATA[");
            xml.append(feed.getTitle());
            xml.append("]]></TITLE>");
            xml.append("<DESCRIPTION><![CDATA[");
            xml.append(feed.getDescription());
            xml.append("]]></DESCRIPTION><LINK><![CDATA[");
            xml.append(feed.getLink());
            xml.append("]]></LINK><DATE><![CDATA[");
            xml.append(feed.getDate());
            xml.append("]]></DATE><CONTENTS><![CDATA[");
            xml.append(feed.getContents());
            xml.append("]]></CONTENTS><ORIGINAL_URL><![CDATA[");
            xml.append(feed.getOriginalUrl());
            xml.append("]]></ORIGINAL_URL><CATEGORY><![CDATA[");
            xml.append(feed.getCategory());
            xml.append("]]></CATEGORY>");
            xml.append("<PERSON_RELATED_KEYWORDS><![CDATA[");
            xml.append(feed.toString(feed.getPersonRelatedKwds()));
            xml.append("]]></PERSON_RELATED_KEYWORDS>");
            xml.append("<ORG_RELATED_KEYWORDS><![CDATA[");
            xml.append(feed.toString(feed.getOrgRelatedKwds()));
            xml.append("]]></ORG_RELATED_KEYWORDS>");
            xml.append("<PLACE_RELATED_KEYWORDS><![CDATA[");
            xml.append(feed.toString(feed.getPlaceRelatedKwds()));
            xml.append("]]></PLACE_RELATED_KEYWORDS>");
            xml.append("<IMG_SRC><![CDATA[");
            xml.append(feed.getImage().getImageSrc());
            xml.append("]]></IMG_SRC>");
            xml.append("<WIDTH><![CDATA[");
            xml.append(feed.getImage().getWidth());
            xml.append("]]></WIDTH>");
            xml.append("<HEIGHT><![CDATA[");
            xml.append(feed.getImage().getHeight());
            xml.append("]]></HEIGHT>");
            xml.append("</ROW>");
        }

        xml.append("</ROOT>");
        return xml.toString();
    }


    public static void main(String[] args) throws Exception {
        String[] x = new String[]{"aaaaa", "bbbb"};
        ArrayList<String> s = new ArrayList<String>();
        s.addAll(Arrays.asList(x));
        ArrayList<Feed> feeds = new ArrayList<Feed>();
        Feed f = new Feed(new UrlCategory("Top Stories", "1", 1), "Taj mahal oviya kadhal", "c", "d", new Date(), s);
        Feed ff = new Feed(new UrlCategory("Top Stories", "1", 1), "Chotta chotta nanayudhu taj mahal", "n", "o", new Date(), s);
        f.setImage(new ImageBean("http://images3.wikia.nocookie.net/__cb20120424100016/india/images/d/d1/Taj-mahal-india-taj-mahal-720x1280.jpg", 720, 1280));
        ff.setImage(new ImageBean("http://images3.wikia.nocookie.net/__cb20120424100016/india/images/d/d1/Taj-mahal-india-taj-mahal-720x1280.jpg", 720, 1280));
        String[] p = new String[]{"org1", "org2"};
        String[] p1 = new String[]{"pla1", "pla2"};
        String[] p2 = new String[]{"per1", "per2"};
        ArrayList<String> pp = new ArrayList<String>();
        pp.addAll(Arrays.asList(p));
        ArrayList<String> pp1 = new ArrayList<String>();
        pp1.addAll(Arrays.asList(p1));
        System.out.println(pp.get(0));
        ArrayList<String> pp2 = new ArrayList<String>();
        pp2.addAll(Arrays.asList(p2));
        f.setOrgRelatedKwds(pp);
        ff.setOrgRelatedKwds(pp);
        ff.setPlaceRelatedKwds(pp1);
        f.setPlaceRelatedKwds(pp1);
        ff.setPersonRelatedKwds(pp2);
        f.setPersonRelatedKwds(pp2);
        f.setOriginalUrl("orig1");
        f.setContents("11");
        ff.setContents("22");
        ff.setOriginalUrl("orig2");
        feeds.add(f);
        feeds.add(ff);
        addFeeds(feeds);
    }

}
