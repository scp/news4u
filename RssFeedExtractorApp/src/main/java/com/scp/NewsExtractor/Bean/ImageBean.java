package com.scp.NewsExtractor.Bean;

/**
 * User: prashanth.v
 * Date: 3/2/13
 * Time: 10:44 PM
 */
public class ImageBean {
    String imageSrc;
    int width;
    int height;

    @Override
    public String toString() {
        return "ImageBean{" +
                "imageSrc='" + imageSrc + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public ImageBean(String imageSrc, int width, int height) {
        this.imageSrc = imageSrc;
        this.width = width;
        this.height = height;
    }

    public ImageBean(String imageSrc) {
        this.imageSrc = imageSrc;
        width=-1;
        height=-1;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
