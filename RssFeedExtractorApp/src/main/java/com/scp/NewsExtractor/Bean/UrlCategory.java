package com.scp.NewsExtractor.Bean;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class UrlCategory {

    String category;
    String url;
    int limit;

    public UrlCategory(String category, String url, int limit) {
        this.category = category;
        this.url = url;
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "UrlCategory{" +
                "url='" + url + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
