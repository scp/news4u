package com.scp.NewsExtractor.Bean;

import com.scp.NewsExtractor.util.ApplicationProperties;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:05 PM
 */
public enum Constants {
    SITE_LINKS("/site-links.xml"),
    INDEX_PATH(ApplicationProperties.getProperty("INDEX_PATH")),
    DOWNLOAD_THREAD_COUNT(50),
    STOP_LIST(ApplicationProperties.getStopList()),
    WIKI_BASE_URL("http://en.wikipedia.org/w/api.php?format=json&action="),
    PLACE_KEYWORD_PREFIXES("Know More About %s", "Wanna Know About %s ?", "Learn More On %s ", "Visit %s", "Things to Know About %s", "Find ways to %s"),
    NAME_KEYWORD_PREFIXES("Get To Know About %s", "What is interesting about %s ?", "New About %s", "Latest on %s"),
    ORG_KEYWORD_PREFIXES("Be a Part of %s", "What is %s ?", "Join %s", "Know about %s"),
    PLACE_TYPE(1),
    NAME_TYPE(2),
    ORG_TYPE(3),
    MISC_TYPE(4),
    CLASSIFIER(CRFClassifier.getClassifierNoExceptions("english.conll.4class.nodistsim.crf.ser.gz"));

    private String stringValue;
    private int numericValue;
    private List<String> stringList;
    private AbstractSequenceClassifier classifier;

    private Constants(List<String> stringList) {
        this.stringList = stringList;
    }

    private Constants(String... stringList) {
        this.stringList = new ArrayList<String>();
        Collections.addAll(this.stringList, stringList);
    }

    Constants(AbstractSequenceClassifier classifier) {
        this.classifier = classifier;
    }


    Constants(String value) {
        this.stringValue = value;
    }

    Constants(int value) {
        this.numericValue = value;
    }

    public int getNumericValue() {
        return numericValue;
    }

    public void setNumericValue(int numericValue) {
        this.numericValue = numericValue;
    }

    public String getStringValue() {
        return stringValue;
    }


    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public AbstractSequenceClassifier getClassifier() {
        return classifier;
    }


}
