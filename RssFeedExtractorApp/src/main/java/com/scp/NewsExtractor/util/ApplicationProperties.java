package com.scp.NewsExtractor.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:00 PM
 */
public class ApplicationProperties {
    private static Logger log = Logger.getLogger(ApplicationProperties.class.getName());
    private static ApplicationProperties applicationProperties = null;
    private static Properties properties = new Properties();
    final private static String STOP_FILE="/en.txt";
    final private static List<String> stopList=new ArrayList<String>();
    final private static String PROPERTY_FILE = "/application.properties";

    static
    {
        getInstance();
    }

    private ApplicationProperties()
    {
        loadProperties();
    }

    public static synchronized ApplicationProperties getInstance()
    {
        if (applicationProperties == null)
        {
            applicationProperties = new ApplicationProperties();
            return applicationProperties;
        }
        else
        {
            return applicationProperties;
        }
    }

    private void loadProperties()
    {

        log.debug("Inside loadProperties");
        try
        {
            loadStopList();
            properties.load( this.getClass().getResourceAsStream( PROPERTY_FILE ) );
        }
        catch (Exception exception)
        {
            log.error( exception.getMessage(), exception );
            throw new RuntimeException( exception.toString() );
        }

        log.info(properties.size() + " Application Properties Loaded!!!");
    }

    private void loadStopList() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(STOP_FILE), "UTF-8"));
        String line = bufferedReader.readLine();
        while (line != null) {
            line = bufferedReader.readLine();
            stopList.add(line);
        }
    }

    public static String getProperty( String key )
    {
        return (String) properties.get( key );
    }

    public static String getProperty( String key, String defaultValue )
    {
        return properties.getProperty( key, defaultValue );
    }
    public static List<String> getStopList(){
        return stopList;
    }

    public static void main(String[] args) {
        ApplicationProperties.getInstance().loadProperties();
    }
}
