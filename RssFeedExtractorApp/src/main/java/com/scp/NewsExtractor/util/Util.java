package com.scp.NewsExtractor.util;

import com.scp.NewsExtractor.Bean.Constants;
import com.scp.NewsExtractor.Bean.SiteDetail;
import com.scp.NewsExtractor.Bean.UrlCategory;
import com.scp.NewsExtractor.RssFeedsAccumulatorApp.ProcessingQueues;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * User: prashanth.v
 * Date: 2/20/13
 * Time: 12:23 PM
 */
public class Util {
    private static Logger log = Logger.getLogger(Util.class);

    public static Object[] readSiteXml(String file) {
        SAXBuilder builder = new SAXBuilder();
        ArrayList<SiteDetail> siteDetails = new ArrayList<SiteDetail>();
        int totalFeedsExpected = 0;
        System.out.println(Constants.SITE_LINKS.getStringValue());
        InputStream xmlFile = Util.class.getResourceAsStream(Constants.SITE_LINKS.getStringValue());
        try {
            if(xmlFile==null){
                System.out.println("NULL");}
            Document document = builder.build(xmlFile);
            Element rootNode = document.getRootElement();
            List list = rootNode.getChildren("site");
            for (Object siteNode : list) {
                Element node = (Element) siteNode;
                String name = node.getChildText("name");
                String baseurl = node.getChildText("baseurl");
                ArrayList<UrlCategory> categoriesUrlList = new ArrayList<UrlCategory>();
                Element categoriesNode = node.getChild("categories");
                List categoryList = categoriesNode.getChildren("category");
                for (Element categoryNode : (List<Element>) categoryList) {
                    String category = categoryNode.getChildText("categoryname");
                    String url = categoryNode.getChildText("categorylink");
                    String limitString = categoryNode.getChildText("limit");
                    int limit = limitString != null ? Integer.parseInt(limitString) : 0;
                    totalFeedsExpected += limit;
                    categoriesUrlList.add(new UrlCategory(category, url, limit));
                    System.out.println(new UrlCategory(category, url, limit).toString());
                }
                Element image = node.getChild("image");
                String attr = image.getChildText("attr");
                String val = image.getChildText("val");
                int linkAvailable = Integer.parseInt(node.getChildText("linkAvailability"));
                SiteDetail siteDetail = new SiteDetail(name, categoriesUrlList, baseurl, attr, val, linkAvailable);
                ProcessingQueues.getInstance().enqueueForFeedExtraction(siteDetail);
                siteDetails.add(siteDetail);
                log.info("ADDED DETAILS FOR " + name);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return new Object[]{siteDetails, totalFeedsExpected};
    }

    public static String isStringSet(String str) {

        if (str == null || str.trim().length() == 0 || str.trim().equalsIgnoreCase("null")) {
            return null;
        } else {
            return str.trim();
        }
    }

    public static int LCS(String X, String Y) {
        final int MAX = 505;
        int[][] L;
        int[][] D;

        L = new int[X.length() + 2][Y.length() + 2];
        D = new int[X.length() + 2][Y.length() + 2];
        int m = X.length();
        int n = Y.length();

        for (int i = 1; i <= m; i++) L[i][0] = 0;
        for (int j = 0; j <= n; j++) L[0][j] = 0;

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (X.charAt(i - 1) == Y.charAt(j - 1)) {
                    L[i][j] = L[i - 1][j - 1] + 1;
                    D[i][j] = 1;
                } else if (L[i - 1][j] >= L[i][j - 1]) {
                    L[i][j] = L[i - 1][j];
                    D[i][j] = 2;
                } else {
                    L[i][j] = L[i][j - 1];
                    D[i][j] = 3;
                }
            }
        }
        return L[m][n];
    }

    public static int getKeywordType(String key) {
        if (key.contains("loc"))
            return Constants.PLACE_TYPE.getNumericValue();
        else if (key.contains("org"))
            return Constants.ORG_TYPE.getNumericValue();
        else if (key.contains("name"))
            return Constants.NAME_TYPE.getNumericValue();
        else
            return Constants.MISC_TYPE.getNumericValue();
    }


    public static void main(String[] args) {
        readSiteXml(Constants.SITE_LINKS.getStringValue());
    }
}
